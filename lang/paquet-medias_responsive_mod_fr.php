<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// L
	'medias_responsive_mod_description' => 'Il ajoute au raccourci &lt;img&gt; un balisage moderne, une collection de paramètres (largeur=xxx, rond, flip, etc.).<br>Il offre des raccourcis supplémentaires : &lt;ligne&gt; destiné  à afficher des images côte à côte et &lt;slide&gt; destiné à afficher des images dans un slider horizontal.',
	'medias_responsive_mod_nom' => 'Insertion avancée d’images',
	'medias_responsive_mod_slogan' => 'Modernise le raccourci &lt;img&gt; et plus.',
);