<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// On déclare les nouveaux modèles de documents pour qu'ils soient prix en compte avec le critère {vu}
function medias_responsive_mod_declarer_tables_objets_sql($tables) {
	$tables['spip_documents']['modeles'][] = 'ligne';
	$tables['spip_documents']['modeles'][] = 'slide';
	$tables['spip_documents']['modeles'][] = 'liste';
	return $tables;
}

function medias_responsive_mod_insert_head_css($flux) {
	$flux = "\n<link rel='stylesheet' type='text/css' media='all' href='".find_in_path("css/medias_responsive.css")."'>\n".$flux;
	return $flux;
}

function medias_responsive_mod_insert_head($flux) {
	$flux .= "<script type='text/javascript' src='".find_in_path("javascript/rAF.js")."'></script>\n";
	$flux .= "<script type='text/javascript' src='".find_in_path("javascript/portfolio_ligne.js")."'></script>\n";
	$flux .= "<script type='text/javascript' src='".find_in_path("javascript/portfolio_slide.js")."'></script>\n";
	$flux .= "<script type='text/javascript' src='".find_in_path("javascript/medias_responsive.js")."'></script>\n";
	return $flux;
}

function medias_responsive_mod_header_prive($flux) {
	$flux .= "<script type='text/javascript' src='".find_in_path("javascript/rAF.js")."'></script>\n";
	$flux .= "<script type='text/javascript' src='".find_in_path("javascript/portfolio_ligne.js")."'></script>\n";
	$flux .= "<script type='text/javascript' src='".find_in_path("javascript/portfolio_slide.js")."'></script>\n";
	$flux .= "<script type='text/javascript' src='".find_in_path("javascript/medias_responsive.js")."'></script>\n";
	$flux .= "\n<link rel=\"stylesheet\" type=\"text/css\" href=\"".find_in_path("css/medias_responsive.css")."\">\n";
	return $flux;
}

function medias_responsive_callback_initier_portfolio_slide($matches) {
	$m = $matches[0];
	
	$rand = rand(0,100000);
	if (preg_match_all(",<li>,s", $m, $res)){
		$nombre = count($res[0]);
	}
	$chk = "";
	$nav = "";
	$styles = "input.portfolio_slide_radio.sel0:checked ~ .label_ligne_0 { display: block; }\n";
	$styles = "[dir=rtl] ul.portfolio_slide li+li { transform:translate3d(-104%,0,0); }\n";

	for ( $i = 0; $i < $nombre; $i++) {
		if ($i == 0) $checked=" checked";
		else $checked = "";
		$iplus = $i+1;
		$decal = $i*104;
		$styles .= "ul.portfolio_slide > li:nth-child($iplus) { transform:translate3d($decal%,0,0); }\n";
		$styles .= "[dir=rtl] ul.portfolio_slide li:nth-child($iplus) { transform:translate3d(-$decal%,0,0); }\n";
		$styles .= "input.portfolio_slide_radio.sel$i:checked ~ ul.portfolio_slide { transform: translate3d(-$decal%,0,0); }\n";
		$styles .= "[dir=rtl] input.portfolio_slide_radio.sel$i:checked ~ ul.portfolio_slide { transform: translate3d($decal%,0,0); }\n";
		$styles .= "input.portfolio_slide_radio.sel$i:checked ~ .label_ligne_$i { display: block; }\n";
		$chk .= "<input type='radio' id='check_ligne_$rand$i' class='portfolio_slide_radio sel$i' name='check_ligne_$rand' value='$i'$checked>";
		if ($i > 0) $nav .= "<label for='check_ligne_$rand".($i-1)."' class='label_ligne label_ligne_precedent label_ligne_$i'><span>"._T('precedent')."</span></label>";
		if ($i < $nombre-1) $nav .= "<label for='check_ligne_$rand".($i+1)."' class='label_ligne label_ligne_suivant label_ligne_$i'><span>"._T('suivant')."</span></label>";
	}
	return "<div class=\"portfolio_slide_container\"><style>$styles</style>".$chk.$m.$nav."</div>";
}

function medias_responsive_mod_affichage_final($txt) {

	// Corriger incompatibilité avec Typo enluminée qui ajoute des <p> </p>
	if (test_plugin_actif('typoenluminee')) $txt = preg_replace(",<p>\ *<\/p>,", "", $txt);

	$txt = preg_replace (",</ul>[\r\t\n\ ]*<ul class=\"portfolio_ligne\">,", "", $txt);

	$txt = preg_replace (",</ul>[\r\t\n\ ]*<ul class=\"portfolio_slide\">,", "", $txt);
	$txt = preg_replace_callback(
		",<ul class=\"portfolio_slide\">(.*)<\/ul>,sU", 
		"medias_responsive_callback_initier_portfolio_slide",
		$txt);
	return $txt;
}