
function calculer_spip_documents() {

	$(".spip_documents").each(function() {
		var t = $(this);
		
		var width = parseInt(t.attr("data-w"));
		var parent = parseInt(t.parent().innerWidth());
			
		if (width > parent) t.width("auto");
		else t.width(width);
			
		if ( t.hasClass("spip_documents_right") || t.hasClass("spip_documents_left") ) {	
			var l = width;
			l = l + parseInt(t.css("marginRight"));
			l = l + parseInt(t.css("marginLeft"));
			
			if (l > 0.6*parent) t.addClass("spip_documents_center_forcer").css("width", "auto");
			else t.removeClass("spip_documents_center_forcer").css("width", width+"px");
			
			console.log(t);
			console.log(width + " / " + l + " / " + parseInt(t.css("marginRight")) + " / " + parseInt(t.css("marginLeft")) ) ;

		}
	});
}


var medias_resp_a_observer = $(".spip_documents.kenburns, .spip_documents_flip");
if('IntersectionObserver' in window){
    var medias_resp_obs = new IntersectionObserver(medias_resp_observerCallback);

    function medias_resp_observerCallback(entries, observer) {
        entries.forEach(entry => {
            if (entry.isIntersecting) {
				if (entry.target.classList.contains('spip_documents_flip')) {
					entry.target.classList.add("spip_documents_flip_actif");
				}
				if (entry.target.classList.contains('kenburns')) {
					entry.target.classList.add("kenburns_actif");
				}

            }
        });
    };
    medias_resp_a_observer.each(function(){
        medias_resp_obs.observe(this);
    });
    
} else { /* Si pas de insersectionObserver pour ce navigateur */
	medias_resp_a_observer.each(function(){
		if($(this).hasClass("spip_documents_flip")) $(this).addClass("spip_documents_flip_actif");
		if($(this).hasClass("kenburns")) $(this).addClass("kenburns_actif");
	});
}

$(document).ready(calculer_spip_documents);
$(window).smartresize(calculer_spip_documents);
